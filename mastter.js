customElements.define("prof-msd", class extends HTMLElement {
	connectedCallback() {
		this.innerHTML = `
			<div class="prof">増</div>
			<div class="status">
				<div>@masuda</div>
				<div><a href="https://anond.hatelabo.jp/">https://anond.hatelabo.jp/</a></div>
			</div>
		`
		this.querySelector(".prof").addEventListener("click", () => {
			document.documentElement.scrollTo({ top:0, behavior: "smooth" })
		})
	}
})

customElements.define("write-msd", class extends HTMLElement {
	connectedCallback() {
		this.innerHTML = `
			<div class="write-msd">
				<textarea class="ta"></textarea>
				<div class="btm">
					<span class="num">0 / 300</span>
					<button name="send">投稿する</button>
				</div>
			</div>
		`
		this.addEventListener("input", eve => {
			const num = eve.target.value.length
			const elem = this.querySelector(".num")
			elem.textContent = `${num} / 300`
			elem.classList.toggle("over", num > 300)
		})
		this.addEventListener("click", eve => {
			const text = this.querySelector(".ta").value
			if (text.trim() && eve.target.name === "send") {
				if (confirm("投稿しますか？")) {
					const [title, body] = this.detectTitle(text)
					new Scraper().send(title, body).then(
						() => {
							alert("投稿しました")
						},
						(err) => {
							alert("投稿に失敗しました")
							console.error(err)
						}
					)
				}
			}
		})
	}
	
	detectTitle(text) {
		const [first, second] = text.split("\n", 2)
		if(second === "") {
			return [first, text.slice(first.length + 2)]
		} else {
			return ["", text]
		}
	}
})

customElements.define("list-msd", class extends HTMLElement {
	only_mine = true
	next_page = 1

	connectedCallback() {
		this.innerHTML = `
			<div class="list-msd">
				<div class="opt">
					<label><input type="radio" name="type" value="0"> 全増田</label>
					<label><input type="radio" name="type" value="1" checked> マイ増田</label>
					<button class="reload">↻</button>
				</div>
				<div class="list"></div>
				<div class="next">続きを取得</div>
			</div>
		`
		this.querySelector(".opt").addEventListener("change", () => {
			this.only_mine = !!+this.querySelector("[name=type]:checked").value
			this.reset()
		})
		this.querySelector(".reload").addEventListener("click", () => this.reset())
		this.querySelector(".next").addEventListener("click", () => {
			this.getMore()
		})
		Promise.resolve().then(() => this.reset())
	}

	async getMore() {
		let list
		const next_page = this.next_page++
		if(this.only_mine) {
			list = await new Scraper().getMyList(next_page)
		} else {
			list = await new Scraper().getGlobalList(next_page)
		}
		
		const div = this.querySelector(".list")
		if(div.querySelector(".loading")) {
			div.innerHTML = ""
			this.querySelector(".next").hidden = false
		}
		
		for(const item of list) {
			if(this.querySelector(`article[id="${item.id}"]`)) continue

			const sub = document.createElement("article")
			sub.dataset.id = item.id
			sub.innerHTML = `
				<h1 class="title"><a></a></h1>
				<div class="body"></div>
			`
			sub.querySelector(".title a").textContent = "■ " + item.title
			sub.querySelector(".title a").href = "https://anond.hatelabo.jp/" + item.id
			sub.querySelector(".body").innerHTML = item.body
			div.append(sub)
		}
	}
	
	reset() {
		this.next_page = 1
		this.querySelector(".list").innerHTML = `<div class="loading">↻</div>`
		this.querySelector(".next").hidden = true
		this.getMore()
	}
})

class Scraper {
	async getDoc(url) {
		return fetch(url)
			.then(e => e.text())
			.then(html => new DOMParser().parseFromString(html, "text/html"))
	}

	async getUserBase() {
		const top = "https://anond.hatelabo.jp/"
		const doc = await this.getDoc(top)
		const elem = doc.querySelector(".username a")
		if(!elem) throw { error: "ログインしていません" }
		return new URL(elem.getAttribute("href"), top)
	}

	async send(title, body) {
		const baseurl = await this.getUserBase()
		const editurl = baseurl.href + "edit"
		const doc = await this.getDoc(editurl)
		const rkm = doc.querySelector("#body form").elements.rkm.value
		const data = {
			rkm,
			mode: "confirm",
			id: "",
			title: title,
			body: body,
			edit: "この内容を登録する",
		}
		const res = await fetch(editurl, {
			method: "POST",
			body: new URLSearchParams(data).toString(),
			headers: {
				"content-type": "application/x-www-form-urlencoded"
			}
		})
		console.log(res)
	}

	parseItems(doc) {
		const sections = doc.querySelectorAll(".day .body .section")
		return [...sections].map(sec => {
			const id = sec.querySelector("a").getAttribute("href").slice(1)
			const title = sec.querySelector(":scope>h3").textContent.slice(1).trim()
			const removes = sec.querySelectorAll(":scope>h3,:scope>.afc,:scope>.share-button,:scope>.share-button~*")
			for(const rem of removes) rem.remove()
			const body = sec.innerHTML.replace(/href="\//g, `href="https://anond.hatelabo.jp/`)
			return { id, title, body }
		})
	}

	async getMyList(page) {
		const baseurl = await this.getUserBase()
		baseurl.searchParams.set("page", page)
		const doc = await this.getDoc(baseurl)
		return this.parseItems(doc)
	}
	
	async getGlobalList(page) {
		const baseurl = new URL("https://anond.hatelabo.jp/")
		baseurl.searchParams.set("page", page)
		const doc = await this.getDoc(baseurl)
		return this.parseItems(doc)
	}
}
